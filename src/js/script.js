// import $ from 'jquery'
// import ready from './utils/documentReady';
import LocomotiveScroll from 'locomotive-scroll';
import preloadImages from './utils/preloadImages';
import {
  Swiper,
  Navigation,
  Pagination,
  Scrollbar,
  EffectFade,
  Autoplay
} from 'swiper';



// Preload images
preloadImages().then(() => {

  // Initialize the Locomotive scroll
  setTimeout(function () {
    document.body.classList.remove('loading');
    const scroll = new LocomotiveScroll({
      el: document.querySelector('[data-scroll-container]'),
      smooth: true,
      reloadOnContextChange: true
    });
    scroll.update();
  }, 2000);


  // let getInTouch = document.querySelector('.get-a-call');
  let getInTouchModal = document.querySelector('.get-in-touch');


  document.addEventListener('click', function () {
    if ( !event.target.classList.contains('get-a-call__link') ) return;
    let getInTouchModal = document.querySelector(event.target.hash);
    if ( !getInTouchModal ) return;
    event.preventDefault();
    if ( getInTouchModal.classList.contains('get-in-touch--is-active') ) return;
    getInTouchModal.classList.add('get-in-touch--is-active');
  });



  document.addEventListener('click', function () {
    if ( !event.target.classList.contains('get-in-touch__close') ) return;
    getInTouchModal.classList.remove('get-in-touch--is-active');
  });


  Swiper.use([
    Navigation,
    Pagination,
    Scrollbar,
    EffectFade,
    Autoplay
  ]);

  var swiper = new Swiper ('.featured-projects__slider', {
    pagination: {
      el: '.featured-projects__slider-progressbar',
      type: 'progressbar'
    },
    navigation: {
      prevEl: '.featured-projects__slider-navigation .slider-navigation__btn--prev',
      nextEl: '.featured-projects__slider-navigation .slider-navigation__btn--next'
    },
    breakpoints: {
      320: {
        slidesPerView: 1,
        spaceBetween: 15
      },
      600: {
        slidesPerView: 2,
        spaceBetween: 15
      }
    }
  });

  var swiper = new Swiper ('.featured-clients__feedback-slider', {
    autoplay: {
      delay: 7500,
    },
    pagination: {
      el: '.featured-clients__slider-progressbar',
      type: 'progressbar'
    },
  });

  var swiper = new Swiper ('.work__gallery-slider', {
    navigation: {
      prevEl: '.work__gallery-navigation .slider-navigation__btn--prev',
      nextEl: '.work__gallery-navigation .slider-navigation__btn--next'
    },
    breakpoints: {
      320: {
        slidesPerView: 1,
        spaceBetween: 15
      },
      600: {
        slidesPerView: 2,
        spaceBetween: 15
      }
    }
  });
});